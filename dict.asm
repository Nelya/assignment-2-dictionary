section .text

global find_word

%include 'lib.inc'
%define ID 8;

find_word:
   .loop:
       push rsi            
       push rdi            
       add rsi, ID      
       call string_equals  
       pop rdi             
       pop rsi          
       test rax, rax       
       jnz .found          
       mov rsi, [rsi]      
       test rsi, rsi
       jnz .loop          
       xor rax, rax        
       ret   
   .found:
       mov rax, rsi        
       ret                  

