section .text

%define BUFFER_LENGTH 256
%define stdout 1
%define stderr 2
%define RETURN_VALUE_ERROR 1
%define ID 8

%include 'words.inc'
%include 'lib.inc'

section .rodata

error_msg: 
    db 'Could not read the word', 10, 0
not_found_msg: 
    db 'The key was not found in the dictionary', 10, 0

section .text

global _start

_start:
    sub rsp, BUFFER_LENGTH
    mov rsi, BUFFER_LENGTH
    mov rdi, rsp
    call read_string
    test rax, rax
    jz .error
    mov rdi, rsp
    mov rsi, first
    call find_word
    add rsp, BUFFER_LENGTH
    test rax, rax
    jz .not_found
    add rax, ID
    mov rdi, rax
    push rax
    call string_length
    pop rsi
    add rsi, rax
    inc rsi
    mov rdi, stdout
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
    .error:
        add rsp, BUFFER_LENGTH
        mov rdi, stderr
        mov rsi, error_msg
        call .exit
    .not_found:
        mov rdi, stderr
        mov rsi, not_found_msg
        call .exit
    .exit:
        call print_string
        mov rdi, RETURN_VALUE_ERROR
        call exit

